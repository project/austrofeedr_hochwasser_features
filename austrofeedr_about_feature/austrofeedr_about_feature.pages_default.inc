<?php
/**
 * @file
 * austrofeedr_about_feature.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function austrofeedr_about_feature_default_page_manager_pages() {
  $page = new stdClass;
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'austrofeedr_about_panel';
  $page->task = 'page';
  $page->admin_title = 'AustroFeedr About Panel';
  $page->admin_description = '';
  $page->path = 'about';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'About',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_austrofeedr_about_panel_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'austrofeedr_about_panel';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display;
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass;
    $pane->pid = 'new-1';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => 'Zustellung von Katastrophenwarnungen und zeitkritischen Nachrichten in Echtzeit

Nachrichten in Echtzeit spielen in Zukunft eine immer wichtigere Rolle, ob bei Unwetter‐ und Lawinenwarnungen, Zivil‐ und Katastrophenschutz, oder bei Twitter, Facebook und den neuen Location‐Based Services wie Gowalla. Bisher war das Internet nicht in der Lage, solche Anforderungen effizient umzusetzen. Dies ändert sich jetzt mit dem neuen, offenen Standard PubSubHubbub, welcher bereits von großen Internet‐Unternehmen wie Google aktiv forciert und von vielen Experten (u.a. Tim O\'Reilly) als "The Next Big Thing" gesehen wird. Wir stehen vor einem Paradigmenwechsel von Pull zu Push, was die Grundlage für das Real‐Time‐Web darstellt. Im Rahmen dieses Projektes soll auf Basis dieses neuen Standards ein universeller und quelloffener Nachrichten‐Hub zur Echtzeit‐Verteilung von Information speziell für die österreichischen Bedürfnisse und Anforderungen implementiert werden.
',
      'format' => 'plain_text',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content['new-1'] = $pane;
    $display->panels['middle'][0] = 'new-1';
    $pane = new stdClass;
    $pane->pid = 'new-2';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'af_about_feature_listing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $display->content['new-2'] = $pane;
    $display->panels['middle'][1] = 'new-2';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-2';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['austrofeedr_about_panel'] = $page;

  return $pages;

}
